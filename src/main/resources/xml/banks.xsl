<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <div style="background-color: green; color: black;">
                    <h2>Bank deposits</h2>
                </div>
                <table border="3">
                    <tr bgcolor="#2E9AFE">
                        <th>Name</th>
                        <th>Country</th>
                        <th>Type</th>
                        <th>Depositor</th>
                        <th>Account Id</th>
                        <th>Amount on deposit</th>
                        <th>Profitability</th>
                        <th>Time constraints</th>
                    </tr>

                    <xsl:for-each select="banks/bank">

                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="country"/></td>
                            <td><xsl:value-of select="type"/></td>
                            <td><xsl:value-of select="depositor"/></td>
                            <td><xsl:value-of select="accountId"/></td>
                            <td><xsl:value-of select="amountOnDeposit"/></td>
                            <td><xsl:value-of select="profitability"/></td>
                            <td><xsl:value-of select="timeConstraints"/></td>
                        </tr>

                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
