package com.khrystyna.views;

import com.khrystyna.comparator.BankComparator;
import com.khrystyna.controller.Controller;
import com.khrystyna.model.Bank;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;

public class BankView implements View {
    private static Logger logger = LogManager.getLogger(BankView.class);
    private Controller controller = new Controller();
    private File xml = new File("src\\main\\resources\\xml\\banks.xml");
    private File xsd = new File("src\\main\\resources\\xml\\banks.xsd");

    @Override
    public void run() {
        printList(controller.getBanksUsingSAX(xml, xsd), "SAX");
        printList(controller.getBanksUsingStAX(xml, xsd), "StAX");
        printList(controller.getBanksUsingDOM(xml, xsd), "DOM");
    }

    private void printList(List<Bank> banks, String parserName) {
        banks.sort(new BankComparator());
        System.out.println(parserName);
        for (Bank bank : banks) {
            logger.trace(bank);
        }
    }
}
