package com.khrystyna.controller;

import com.khrystyna.model.Bank;
import com.khrystyna.parser.dom.DOMBeerParser;
import com.khrystyna.parser.sax.SAXBankParser;
import com.khrystyna.parser.stax.StAXBeerParser;

import java.io.File;
import java.util.List;

public class Controller {
    public List<Bank> getBanksUsingSAX(File xml, File xsd) {
        return SAXBankParser.parse(xml, xsd);
    }

    public List<Bank> getBanksUsingStAX(File xml, File xsd) {
        return StAXBeerParser.parse(xml, xsd);
    }

    public List<Bank> getBanksUsingDOM(File xml, File xsd) {
        return DOMBeerParser.parse(xml, xsd);
    }
}
