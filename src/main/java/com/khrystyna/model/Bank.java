package com.khrystyna.model;

import java.util.Objects;

public class Bank {
    private String name;
    private String country;
    private String type;
    private String depositor;
    private int accountId;
    private double amountOnDeposit;
    private double profitability;
    private int timeConstraints;

    public Bank() {
    }

    public Bank(String name, String country, String type, String depositor, int accountId, double amountOnDeposit, double profitability, int timeConstraints) {
        this.name = name;
        this.country = country;
        this.type = type;
        this.depositor = depositor;
        this.accountId = accountId;
        this.amountOnDeposit = amountOnDeposit;
        this.profitability = profitability;
        this.timeConstraints = timeConstraints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDepositor() {
        return depositor;
    }

    public void setDepositor(String depositor) {
        this.depositor = depositor;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public double getAmountOnDeposit() {
        return amountOnDeposit;
    }

    public void setAmountOnDeposit(double amountOnDeposit) {
        this.amountOnDeposit = amountOnDeposit;
    }

    public double getProfitability() {
        return profitability;
    }

    public void setProfitability(double profitability) {
        this.profitability = profitability;
    }

    public int getTimeConstraints() {
        return timeConstraints;
    }

    public void setTimeConstraints(int timeConstraints) {
        this.timeConstraints = timeConstraints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bank banks = (Bank) o;
        return accountId == banks.accountId &&
                Double.compare(banks.amountOnDeposit, amountOnDeposit) == 0 &&
                Double.compare(banks.profitability, profitability) == 0 &&
                timeConstraints == banks.timeConstraints &&
                Objects.equals(name, banks.name) &&
                Objects.equals(country, banks.country) &&
                Objects.equals(type, banks.type) &&
                Objects.equals(depositor, banks.depositor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, country, type, depositor, accountId, amountOnDeposit, profitability, timeConstraints);
    }

    @Override
    public String toString() {
        return "Bank{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", type='" + type + '\'' +
                ", depositor='" + depositor + '\'' +
                ", accountId=" + accountId +
                ", amountOnDeposit=" + amountOnDeposit +
                ", profitability=" + profitability +
                ", timeConstraints=" + timeConstraints +
                '}';
    }
}
