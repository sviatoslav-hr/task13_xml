package com.khrystyna.parser.dom;


import com.khrystyna.model.Bank;
import org.w3c.dom.Document;

import java.io.File;
import java.util.List;

public class DOMBeerParser {
    public static List<Bank> parse(File xml, File xsd){
        DOMDocCreator creator = new DOMDocCreator(xml);
        Document doc = creator.getDocument();

        DOMBankReader reader = new DOMBankReader();

        return reader.readDoc(doc);
    }
}
