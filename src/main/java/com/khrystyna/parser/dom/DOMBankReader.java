package com.khrystyna.parser.dom;


import com.khrystyna.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMBankReader {
    public List<Bank> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Bank> banks = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("bank");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                banks.add(getBankFromElement(element));
            }
        }
        return banks;
    }

    private Bank getBankFromElement(Element element) {
        Bank bank = new Bank();
        bank.setName(element
                .getElementsByTagName("name")
                .item(0)
                .getTextContent());
        bank.setCountry(element
                .getElementsByTagName("country")
                .item(0)
                .getTextContent());
        bank.setType(element
                .getElementsByTagName("type")
                .item(0)
                .getTextContent());
        bank.setDepositor(element
                .getElementsByTagName("depositor")
                .item(0)
                .getTextContent());
        bank.setAccountId(Integer.parseInt(element
                .getElementsByTagName("accountId")
                .item(0)
                .getTextContent()));
        bank.setAmountOnDeposit(Double.parseDouble(element
                .getElementsByTagName("amountOnDeposit")
                .item(0)
                .getTextContent()));
        bank.setProfitability(Double.parseDouble(element
                .getElementsByTagName("profitability")
                .item(0)
                .getTextContent()));
        bank.setType(element
                .getElementsByTagName("timeConstraints")
                .item(0)
                .getTextContent());
        return bank;
    }
}
