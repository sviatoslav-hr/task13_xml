package com.khrystyna.parser.stax;

import com.khrystyna.model.*;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXBeerParser {
    public static List<Bank> parse(File xml, File xsd){
        List<Bank> banks = new ArrayList<>();
        Bank bank = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "bank":
                            xmlEvent = xmlEventReader.nextEvent();
                            bank = new Bank();
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setName(xmlEvent
                                    .asCharacters().getData());
                            break;
                        case "country":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setCountry(xmlEvent.asCharacters().getData());
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setType(xmlEvent.asCharacters().getData());
                            break;
                        case "accountId":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setAccountId(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "amountOnDeposit":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setAmountOnDeposit(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "profitability":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setProfitability(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "timeConstraints":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setTimeConstraints(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("bank")){
                        banks.add(bank);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return banks;
    }
}
