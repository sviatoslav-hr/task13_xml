package com.khrystyna.parser.sax;


import com.khrystyna.model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Bank> banks = new ArrayList<>();
    private Bank bank;

    private boolean hasName;
    private boolean hasCountry;
    private boolean hasType;
    private boolean hasDepositor;
    private boolean hasAccountId;
    private boolean hasAmountOnDeposit;
    private boolean hasProfitability;
    private boolean hasTimeConstraints;

    public List<Bank> getBanks() {
        return this.banks;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("bank")) {
            bank = new Bank();
        } else if (qName.equalsIgnoreCase("name")) {
            hasName = true;
        } else if (qName.equalsIgnoreCase("country")) {
            hasCountry = true;
        } else if (qName.equalsIgnoreCase("type")) {
            hasType = true;
        } else if (qName.equalsIgnoreCase("depositor")) {
            hasDepositor = true;
        } else if (qName.equalsIgnoreCase("accountId")) {
            hasAccountId = true;
        } else if (qName.equalsIgnoreCase("amountOnDeposit")) {
            hasAmountOnDeposit = true;
        } else if (qName.equalsIgnoreCase("profitability")) {
            hasProfitability = true;
        } else if (qName.equalsIgnoreCase("timeConstraints")) {
            hasTimeConstraints = true;
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("bank")) {
            banks.add(bank);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        String value = new String(ch, start, length);
        if (hasName) {
            bank.setName(value);
            hasName = false;
        } else if (hasCountry) {
            bank.setCountry(value);
            hasCountry = false;
        } else if (hasType) {
            bank.setType(value);
            hasType = false;
        } else if (hasAccountId) {
            bank.setAccountId(Integer.parseInt(value));
            hasAccountId = false;
        } else if (hasDepositor) {
            bank.setDepositor(value);
            hasDepositor = false;
        } else if (hasAmountOnDeposit) {
            bank.setAmountOnDeposit(Double.parseDouble(value));
            hasAmountOnDeposit = false;
        } else if (hasProfitability) {
            bank.setProfitability(Double.parseDouble(value));
            hasProfitability = false;
        } else if (hasTimeConstraints) {
            bank.setTimeConstraints(Integer.parseInt(value));
            hasTimeConstraints = false;
        }
    }
}

