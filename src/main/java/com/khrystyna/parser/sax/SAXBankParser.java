package com.khrystyna.parser.sax;


import com.khrystyna.model.Bank;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class SAXBankParser {
    private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

    public static List<Bank> parse(File xml, File xsd) {
        try {
            saxParserFactory.setSchema(SAXValidator.createSchema(xsd));

            SAXParser saxParser = saxParserFactory.newSAXParser();
            SAXHandler saxHandler = new SAXHandler();
            saxParser.parse(xml, saxHandler);

            return saxHandler.getBanks();
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            ex.printStackTrace();
            return Collections.emptyList();
        }
    }
}
